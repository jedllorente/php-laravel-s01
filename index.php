<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=h1, initial-scale=1.0">
    <title>S01: PHP Basics and Selection Control Structures</title>
</head>

<body>
    <h1>Echoing Values</h1>

    <p><?php echo 'Hello World' ?></p>
    <p><?php echo 'Good day $name! Your given email is $email.' ?></p>
    <p><?php echo "Good day $name! Your given email is $email."?></p>
    <p><?php echo PI; ?></p>

    <!-- Data types -->
    <p><?php echo "State is $state is in $country."; ?></p>

    <p><?php echo $address;?></p>
    <p><?php echo $addressTwo;?></p>

    <!-- Echoing Boolean and Null Variables -->
    <!-- Noraml  echoing of Boolean and null Variables will not appear in web output -->
    <p><?php echo $hasTravelledAbroad;?></p>
    <p><?php echo $spouse;?></p>

    <!-- gettype() function returns the type of a variable -->
    <p><?php echo gettype($hasTravelledAbroad);?></p>
    <!-- var_dump() function gives detail about the variable -->
    <p><?php echo var_dump($spouse);?></p>

    <!-- Objects -->
    <p><?php echo $gradesObj -> firstGrading;?></p>
    <p><?php echo $personObj->address->state;?></p>

    <p><?php echo $grades[3];?></p>
    <p><?php echo $grades[0];?></p>

    <!-- Operators -->
    <h1>Operators</h1>
    <p>X: <?php echo $x;?></p>
    <p>Y: <?php echo $y;?></p>

    <p>Is Legal Age: <?php echo var_dump($isLegalAge); ?></p>
    <p>Is Registered: <?php echo var_dump($isRegistered);?> </p>

    <p>Sum: <?php echo $x + $y;?></p>
    <p>Difference: <?php echo $x - $y;?></p>
    <p>Product: <?php echo $x * $y;?></p>
    <p>Quotient: <?php echo $x / $y;?></p>

    <h1>Equality Operators</h1>
    <p>Loose Equality: <?php echo var_dump($x == '56.2');?></p>
    <p>Strict Equality: <?php echo var_dump($x === '56.2');?></p>
    <p>Loose Inequality: <?php echo var_dump($x != '56.2');?></p>
    <p>Strict Inequality: <?php echo var_dump($x !== '56.2');?></p>

    <p>Is Lesser: <?php echo var_dump($x < $y);?></p>
    <p>Is Greater: <?php echo var_dump($x > $y);?></p>

    <h2>Logical Operators</h2>
    <p>Are all requirements Met: <?php echo var_dump($isLegalAge and $isRegistered)?></p>
    <p>Are some requirements Met: <?php echo var_dump($isLegalAge or $isRegistered)?></p>
    <p>Are some requirements not Met: <?php echo var_dump(!$isLegalAge and !$isRegistered)?></p>

    <h1>Functions</h1>
    <p>Full Name: <?php echo getFullName('John', 'S.','Smith')?></p>

    <h1>Selection Control Structure</h1>
    <h2>If Else ElseIf statement</h2>
    <p><?php echo determineTyphoonIntensity(12);?></p>

    <h2>Ternary Sample(Is Underage?)</h2>
    <p>78: <?php echo var_dump(isUnderAge(78));?></p>

    <h2>Switch</h2>
    <p><?php echo determineComputerUser(4);?></p>
</body>

</html>